export class Rolling {
    windowSize: number

    constructor(windowSize: number) {
        this.windowSize = windowSize
    }

    getVideoIds(playlist: PlaylistItem[]): string[] {
        const videoIds: string[] = []
        for (let i = 0; i < Math.min(this.windowSize, playlist.length); i++) {
            const item = playlist[i]
            if (!item.contentDetails) {
                throw new Error('PlaylistItem has no contentDetails')
            }
            videoIds.push(item.contentDetails.videoId)
        }
        return videoIds
    }
}
