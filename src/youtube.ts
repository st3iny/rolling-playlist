import { youtube } from './deps.ts'
import { Config } from './config.ts'

export async function getPlaylists(yt: youtube.YouTube, parts: string[] = []): Promise<any[]> {
  if (!parts.find(part => part === 'id')) {
    parts.push('id')
  }

  const step = 50
  const playlists: any[] = []
  let pageToken: string | undefined
  while (true) {
    const params: youtube.schema_playlists_list = {
      part: parts.join(','),
      mine: true,
      maxResults: step,
    }
    if (pageToken) params.pageToken = pageToken
    const result = await yt.playlists_list(params);
    pageToken = result.nextPageToken
    playlists.push(...result.items)
    if (result.items.length < step) {
      break;
    }
  }

  return playlists
}

export async function getPlaylistItems(yt: youtube.YouTube, playlistId: string, parts: string[] = []): Promise<PlaylistItem[]> {
  if (!parts.find(part => part === 'id')) {
    parts.push('id')
  }

  const step = 50
  let pageToken: string | undefined;
  const items: PlaylistItem[] = []
  while (true) {
    const params: youtube.schema_playlistItems_list = {
      part: parts.join(','),
      playlistId,
      maxResults: step,
    }
    if (pageToken) params.pageToken = pageToken
    const result: PlaylistItems = await yt.playlistItems_list(params);
    pageToken = result.nextPageToken
    items.push(...result.items)
    if (result.items.length < step) {
      break
    }
  }

  return items
}

export async function deletePlaylistItem(config: Config, id: string): Promise<void> {
  const url = new URL('https://www.googleapis.com/youtube/v3/playlistItems')
  url.searchParams.set('key', config.apiKey ?? '')
  url.searchParams.set('id', id)
  await fetch(url, {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${config.accessToken}`,
    }
  })
}
