import { exists } from './util.ts'

const configPath = Deno.env.get('CONFIG_PATH') ?? './config.json'

export class Config {
  // Authentication
  apiKey?: string
  accessCode?: string
  accessToken?: string
  accessTokenExpiry?: number
  refreshToken?: string
  clientId?: string
  clientSecret?: string
  scopes: string[]

  // State
  likesEtag?: string
  videoIds: string[]
  ignoredVideoIds: string[]

  private constructor() {
    this.scopes = [
      'https://www.googleapis.com/auth/youtube',
    ]
    this.videoIds = []
    this.ignoredVideoIds = []
  }

  get redirectUri(): string {
    let host = Deno.env.get('HOST') ?? 'http://localhost:8080/'
    if (!host.endsWith('/')) host += '/'
    return host + 'oauth'
  }

  static async load(): Promise<Config> {
    if (!(await exists(configPath))) {
      return new Config()
    }

    const bytes = await Deno.readFile(configPath)
    const decoder = new TextDecoder('utf-8')
    const data = JSON.parse(decoder.decode(bytes))
    return Object.assign(new Config(), data)
  }

  async save(): Promise<void> {
    const json = JSON.stringify(this)
    const encoder = new TextEncoder()
    const bytes = encoder.encode(json)
    await Deno.writeFile(configPath, bytes)
    await Deno.chmod(configPath, 0o600)
  }
}
