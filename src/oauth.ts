import { now } from './util.ts'
import { Config } from './config.ts'
import { youtube } from './deps.ts'

export async function getToken(config: Config): Promise<any> {
  if (!config.accessCode || !config.clientId || !config.clientSecret) return

  const url = 'https://oauth2.googleapis.com/token'
  const body = new URLSearchParams({
    code: config.accessCode,
    client_id: config.clientId,
    client_secret: config.clientSecret,
    grant_type: 'authorization_code',
    redirect_uri: 'http://localhost:8080/oauth',
  })
  console.log('getToken', body.toString())
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body,
  })

  const token = await response.json()
  console.log('getToken', token)
  if (!token.error) {
    config.accessToken = token.access_token
    config.accessTokenExpiry = Math.floor(now() + token.expires_in)
    config.refreshToken = token.refresh_token
    await config.save()
  }

  return token
}

export async function refreshToken(config: Config): Promise<any> {
  if (!config.clientId || !config.clientSecret || !config.refreshToken) return

  const url = 'https://oauth2.googleapis.com/token'
  const body = new URLSearchParams({
    client_id: config.clientId,
    client_secret: config.clientSecret,
    refresh_token: config.refreshToken,
    grant_type: 'refresh_token',
  })
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body
  })

  const token = await response.json()
  console.log('refreshToken', token)
  if (!token.error) {
    config.accessToken = token.access_token
    config.accessTokenExpiry = Math.floor(now() + token.expires_in)
    await config.save()
  }

  return token
}

export async function checkToken(config: Config): Promise<youtube.YouTube | null> {
  if (!config.apiKey || !config.accessToken || !config.accessTokenExpiry) return null

  if (now() >= config.accessTokenExpiry - 60) {
    console.log('checkToken refreshing token')
    await refreshToken(config)
  }

  if (!config.apiKey || !config.accessToken) {
    return null
  }

  return new youtube.YouTube(config.apiKey, config.accessToken)
}
