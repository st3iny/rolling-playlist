import { oak } from './deps.ts'

export async function exists(path: string): Promise<boolean> {
  try {
    await Deno.stat(path)
    return true
  } catch (error) {
    if (error && error instanceof Deno.errors.NotFound) {
      return false;
    }

    throw error;
  }
}

export function now(): number {
  return new Date().getTime() / 1000
}

export function jsonResponse(ctx: oak.Context, data: any) {
  const searchParams = ctx.request.url.searchParams;
  const pretty = searchParams.has('pretty')

  ctx.response.headers.set('Content-Type', 'application/json')
  if (pretty) {
    ctx.response.body = JSON.stringify(data, null, 2)
  } else {
    ctx.response.body = JSON.stringify(data)
  }
}

