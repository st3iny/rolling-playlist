/// <reference types="./types.d.ts" />

import { oak, youtube } from './deps.ts'
import { responseTimeMiddleware, versionMiddleware } from './middleware.ts'
import { Rolling } from './rolling.ts'
import { jsonResponse } from './util.ts'
import { checkToken, getToken, refreshToken } from './oauth.ts'
import { Config } from './config.ts'
import { deletePlaylistItem, getPlaylistItems, getPlaylists } from './youtube.ts'

const config: Config = await Config.load()

const controller = new AbortController();
const { signal } = controller;

const app = new oak.Application();

app.use(versionMiddleware())
app.use(responseTimeMiddleware({ log: true, header: true}))

const router = new oak.Router()

router.get('/exit', (ctx) => {
  ctx.response.status = 200
  controller.abort()
})

router.get('/health', (ctx) => {
  ctx.response.headers.set('Content-Type', 'text/plain')
  ctx.response.body = 'Alive'
})

router.get('/config', (ctx) => {
  jsonResponse(ctx, {
    ...config,
    redirectUri: config.redirectUri,
  })
})

router.post('/config', async (ctx) => {
  const body = ctx.request.body({
    type: 'json',
  })
  const data = await body.value
  if (!data) return

  if (data.clientId) config.clientId = data.clientId
  if (data.clientSecret) config.clientSecret = data.clientSecret
  if (data.apiKey) config.apiKey = data.apiKey
  await config.save()
  jsonResponse(ctx, config)
})

router.get('/login', (ctx) => {
  if (!config.clientId) return

  const creds: youtube.authParams = {
    client_id: config.clientId,
    redirect_uri: config.redirectUri,
    scope: config.scopes.join(' '),
  }
  const auth = new youtube.authenticator();
  const authUrl = new URL(auth.authenticate(creds));
  authUrl.searchParams.set('response_type', 'code')
  ctx.response.headers.set('Content-Type', 'text/html')
  ctx.response.body = `<a href="${authUrl.toString()}">Login</a>`
})

router.get('/oauth', async (ctx) => {
  const accessCode = ctx.request.url.searchParams.get('code')
  if (accessCode) {
    config.accessCode = accessCode
    await config.save()
  }
  jsonResponse(ctx, { accessCode })
})

router.get('/get-token', async (ctx) => {
  const token = await getToken(config)
  jsonResponse(ctx, { token })
})

router.get('/refresh-token', async (ctx) => {
  const token = await refreshToken(config)
  jsonResponse(ctx, { token })
})

router.get('/playlist-test', async (ctx) => {
  const yt = await checkToken(config)
  if (!yt) return

  const playlists = await yt.playlists_list({
    part: 'id,snippet',
    mine: true,
    maxResults: 100,
  })
  const tekke = playlists.items.find((list: any) => list.snippet.title === 'Tekke')

  const songs = await yt.playlistItems_list({
    part: 'contentDetails',
    playlistId: tekke.id,
    maxResults: 100,
  });

  jsonResponse(ctx, {
    playlists,
    tekke,
    songs,
  })
})

router.get('/playlists', async (ctx) => {
  const yt = await checkToken(config)
  if (!yt) return

  const parts: string[] = []
  const searchParams = ctx.request.url.searchParams;
  if (searchParams.has('content_details')) parts.push('contentDetails')
  if (searchParams.has('snippet')) parts.push('snippet')
  const playlists = await getPlaylists(yt, parts)
  jsonResponse(ctx, { playlists })
})

router.get('/likes', async (ctx) => {
  const yt = await checkToken(config)
  if (!yt) return

  const nextPageToken = ctx.request.url.searchParams.get('next_page_token')

  const params: youtube.schema_playlistItems_list = {
    part: 'snippet,contentDetails',
    playlistId: 'LL',
    maxResults: 10,
  }
  if (nextPageToken) params.pageToken = nextPageToken
  const songs = await yt.playlistItems_list(params);
  jsonResponse(ctx, songs)
})

router.get('/get-video-ids', async (ctx) => {
  const yt = await checkToken(config)
  if (!yt) return

  const step = 50
  let windowSize = 10
  const rawWindowSize = ctx.request.url.searchParams.get('window_size')
  if (rawWindowSize) {
    windowSize = parseInt(rawWindowSize)
  }
  windowSize += config.ignoredVideoIds.length

  let pageToken: string | undefined;
  const items: PlaylistItem[] = []
  while (items.length < windowSize) {
    const next = Math.min(step, windowSize - items.length)
    if (next <= 0) {
      break
    }

    console.log(`/get-video-ids requesting ${next} items using pageToken ${pageToken}`)

    const params: youtube.schema_playlistItems_list = {
      part: 'contentDetails',
      playlistId: 'LL',
      maxResults: next,
    }
    if (pageToken) params.pageToken = pageToken
    const songs: PlaylistItems = await yt.playlistItems_list(params);

    if (!pageToken && songs.etag === config.likesEtag && windowSize === config.videoIds.length) {
      console.log('/get-video-ids no changes')
      jsonResponse(ctx, { videoIds: config.videoIds })
      return
    } else if (!pageToken) {
      config.likesEtag = songs.etag
      await config.save()
    }

    pageToken = songs.nextPageToken
    items.push(...songs.items)
  }

  const rolling = new Rolling(windowSize);
  const videoIds = rolling.getVideoIds(items)
  config.videoIds = videoIds
  config.ignoredVideoIds = config.ignoredVideoIds.filter(ignoredVideoId => {
    return items.find(item => item.contentDetails?.videoId === ignoredVideoId)
  })
  await config.save()
  jsonResponse(ctx, { videoIds })
})

router.get('/update-playlist', async (ctx) => {
  const yt = await checkToken(config)
  if (!yt || !config.videoIds) return

  const searchParams = ctx.request.url.searchParams;
  let playlistId = searchParams.get('playlist_id')
  const playlistTitle = searchParams.get('playlist_title')

  if (playlistTitle) {
    const playlists = await getPlaylists(yt, ['snippet', 'id'])
    for (const playlist of playlists) {
      if (playlist.snippet.title === playlistTitle) {
        playlistId = playlist.id
        console.log(`/update-playlist resolved title ${playlistTitle} to id ${playlistId}`)
        break;
      }
    }
  }

  if (!playlistId) {
    jsonResponse(ctx, { error: 'Expected playlist_id or valid playlist_title'})
    return
  }

  interface Expected {
    kind: string,
    videoId: string
    position: number
  }

  interface Actual extends Expected {
    id: string
  }

  const expectedItems: Expected[] = config.videoIds.map((videoId, index) => ({
    kind: 'youtube#video',
    videoId,
    position: index,
  }))

  const actualVideoIds = await getPlaylistItems(yt, playlistId, ['snippet', 'contentDetails', 'id'])
  const actualItems: Actual[] = actualVideoIds.map(item => ({
    kind: 'youtube#video',
    id: item.id,
    videoId: (item.contentDetails as PlaylistItemContentDetails).videoId,
    position: (item.snippet as PlaylistItemSnippet).position,
  }))

  const itemsToCreate: Expected[] = []
  const itemsToDelete: Actual[] = []
  for (const expected of expectedItems) {
    let found = false
    for (const actual of actualItems) {
      if (actual.videoId === expected.videoId) {
        found = true;
        break;
      }
    }

    if (!found) {
      itemsToCreate.push(expected)
    }
  }

  for (const actual of actualItems) {
    let found = false
    for (const expected of expectedItems) {
      if (actual.videoId === expected.videoId) {
        found = true;
        break;
      }
    }

    if (!found) {
      itemsToDelete.push(actual)
    }
  }

  for (const item of itemsToDelete) {
    await deletePlaylistItem(config, item.id)
    console.log(`/update-playlist deleted item ${item.id}`)
  }

  for (const item of itemsToCreate) {
    if (config.ignoredVideoIds.find(videoId => videoId === item.videoId)) {
      continue
    }

    const params: youtube.schema_playlistItems_insert = {
      part: 'snippet',
    }
    const body = JSON.stringify({
      snippet: {
        playlistId,
        resourceId: {
          kind: item.kind,
          videoId: item.videoId,
        },
      },
    })
    const response = await yt.playlistItems_insert(params, body as unknown as object)
    if (response.error?.code === 404) {
      console.log('/update-playlist ignoring video', item.videoId)
      config.ignoredVideoIds.push(item.videoId)
      await config.save()
    } else {
      console.log(`/update-playlist inserted item `, response)
    }
  }

  jsonResponse(ctx, {
    actualItems,
    expectedItems,
    itemsToCreate,
    itemsToDelete,
  })
})

app.use(router.routes())
app.use(router.allowedMethods())

const hostname = Deno.env.get('LISTEN_HOST') ?? '127.0.0.1'
const port = parseInt(Deno.env.get('LISTEN_PORT') ?? '8080')
console.log(`Listening on ${hostname}:${port}`)
await app.listen({
  hostname,
  port,
  signal,
})
