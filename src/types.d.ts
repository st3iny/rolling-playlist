interface YoutubeApiResource {
  kind: string
  etag: string
  id: string
}

interface PlaylistItemContentDetails {
  videoId: string
  videoPublishedAt: string
}

interface PlaylistItemSnippet {
  position: number
}

interface PlaylistItem extends YoutubeApiResource {
  contentDetails?: PlaylistItemContentDetails
  snippet?: PlaylistItemSnippet
}

interface PlaylistItems extends YoutubeApiResource {
  items: PlaylistItem[],
  nextPageToken?: string,
  prevPageToken?: string,
}
