import { oak } from './deps.ts'

type Middleware = (ctx: oak.Context, next: () => Promise<unknown>) => void;

interface ResponseTimeOptions {
  log?: boolean,
  header?: boolean,
}

export function responseTimeMiddleware(options: ResponseTimeOptions = {}): Middleware {
  const log = options.log ?? false;
  const header = options.header ?? false;

  return oak.composeMiddleware([
    // Logger
    async (ctx, next) => {
      await next();
      const rt = ctx.response.headers.get("X-Response-Time");
      if (log && ctx.request.url.pathname.indexOf('/health') === -1) {
        console.log(`${ctx.request.method} ${ctx.request.url} ${ctx.response.status} - ${rt}`);
      }
    },

    // Timer
    async (ctx, next) => {
      const start = Date.now();
      await next();
      const ms = Date.now() - start;
      if (header) {
        ctx.response.headers.set("X-Response-Time", `${ms}ms`);
      }
    },
  ])
}

export function versionMiddleware(): Middleware {
  return async (ctx, next) => {
    ctx.response.headers.set('X-Deno-Version', Deno.version.deno)
    ctx.response.headers.set('X-V8-Version', Deno.version.v8)
    ctx.response.headers.set('X-TypeScript-Version', Deno.version.typescript)
    await next()
  }
}
